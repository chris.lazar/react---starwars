import React, { Component } from 'react';
import styled from 'styled-components'

const PilotInfoStyle = styled.section`
    margin: 10px;
    width: 200px;
`

class PilotInfo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            favouritePilots: []
        }
    }
    renderPilotInfo = () => {
        return (
            <div>
                <h4>Name: {this.props.showPilotDetails.name}</h4>
                <h5>Height: {this.props.showPilotDetails.height}cm</h5>
            </div>
        )
    }

    addToFavourites = () => {
        if (this.state.favouritePilots.indexOf(this.props.showPilotDetails.name) === -1) {
            const favourites = [...this.state.favouritePilots, ...[this.props.showPilotDetails.name]]
            this.setState({ favouritePilots: favourites })
        }else{
            alert(`${this.props.showPilotDetails.name} is already in your favourites`)
        }
    }

    removeFromFavourites = (pilot) => {
        const pilotToRemove = this.state.favouritePilots.indexOf(pilot);
        const newFavouritePilots = [...this.state.favouritePilots.slice(0, pilotToRemove), ...this.state.favouritePilots.slice(pilotToRemove + 1)]
        this.setState({ favouritePilots: newFavouritePilots })
    }

    showFavouritePilots = () => {
        if (this.state.favouritePilots.length !== 0) {
            return this.state.favouritePilots.map((pilot, index) => {
                return (
                    <div key={index}>
                        {pilot}<button onClick={() => this.removeFromFavourites(pilot)}>X</button>
                    </div>
                )
            })
        } else {
            return "There are no pilots you like!"
        }
    }

    render() {
        return (
            <PilotInfoStyle>
                <h1>Pilot Info</h1>
                <div>{this.renderPilotInfo()}</div>
                <div>
                    <button onClick={() => this.addToFavourites()}>Add to favourite</button>
                </div>
                <div>
                    <h3>Favourite Pilots</h3>
                    {this.showFavouritePilots()}
                </div>
            </PilotInfoStyle>
        )
    }
}
export default PilotInfo;