import React, {Component} from 'react';

import MovieTitle from './MovieTitle'
import '../../components/Movies/styles.scss';

class Movies extends Component {
    constructor(props) {
        super(props);

        this.state = {
            movies: []
        }

        this.baseUrl = 'https://swapi.co/api/';
    }

    loading = () => {
        return (
            <div className="lds-ripple">
                <div> </div>
                <div> </div>
            </div>
        )
    }

    componentDidMount() {
        fetch(`${this.baseUrl}films/`)
            .then(response => response.json())
            .then(data => {
                const movies = data.results
                this.setState({movies: movies})
            })
    }

    render() {
        return this.state.movies ?
            <MovieTitle moviesArray={this.state.movies}/> : this.loading()
    }
}

export default Movies;