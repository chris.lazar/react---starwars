import React, { Component } from 'react';

import Starship from './Starship';
import styled from 'styled-components';

const Wrapper = styled.section`
    display: flex;
`

const ShipListStyle = styled.section`
    margin: 10px;
`

const ShipDetailsStyle = styled.section`
    margin: 10px;
`


class StarshipList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showStarShipDetails: false,
            starshipInfo: ''
        }
    }

    handleClick = e => {
        e.preventDefault();
        const link = e.currentTarget.href;
        fetch(`${link}`)
            .then(response => response.json())
            .then(data => {
                this.setState({ showStarShipDetails: true, starshipInfo: data })
            });
    }

    render() {
        return (
            <Wrapper>
                <ShipListStyle>
                    <h1>Starships List</h1>
                    {
                        this.props.starShips.map(starship => {
                            return (
                                <div key={starship}>
                                    <a href={starship} onClick={(e) => this.handleClick(e)}>{starship}</a>
                                </div>
                            )
                        })
                    }
                </ShipListStyle>
                <ShipDetailsStyle>
                    {this.state.showStarShipDetails ? <Starship starShipDetails={this.state.starshipInfo} /> : null}
                </ShipDetailsStyle>
            </Wrapper>
        )
    }
}

export default StarshipList;