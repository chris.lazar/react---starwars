import React, { Component } from 'react';
import styled from 'styled-components';

import PilotInfo from '../PilotContainer/PilotInfo';

const Wrapper = styled.section`
    display: flex;
`

const PilotNameStyle = styled.section`
    // width: 20vw;
`
const PilotDetailsStyle = styled.section`
    // width: 35vw;

`

class StarShip extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showPilotDetails: false,
            pilotInfo: ''
        }
    }

    handleClick = e => {
        e.preventDefault();
        const link = e.currentTarget.href;
        fetch(`${link}`)
            .then(response => response.json())
            .then(data => {
                this.setState({ showPilotDetails: true, pilotInfo: data })
            })

    }

    renderPilots = () => {
        if (this.props.starShipDetails.pilots.length === 0) {
            return "No pilot information"
        } else {
            return this.props.starShipDetails.pilots.map(pilot => {
                return (
                    <div key={pilot}>
                        <a href={pilot} onClick={(e) => this.handleClick(e)}>{pilot}</a>
                    </div>
                )
            })
        }
    }

    render() {
        return (
            <Wrapper>
                <PilotNameStyle>
                    <h1>Starship Info</h1>
                    <h4>
                        Name: {this.props.starShipDetails.name}
                    </h4>
                    <p>Cost ${this.props.starShipDetails.cost_in_credits}</p>
                    <div>
                        {this.renderPilots()}
                    </div>
                </PilotNameStyle>
                <PilotDetailsStyle>
                    {this.state.showPilotDetails ? <PilotInfo showPilotDetails={this.state.pilotInfo} /> : null}
                </PilotDetailsStyle>
            </Wrapper>
        )
    }
}

export default StarShip;