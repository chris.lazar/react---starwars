import React, {Component} from 'react';

import Movies from '../../containers/MoviesContainer/Movies'
import styled from 'styled-components';
import StarWarsGalaxy from '../../assets/star-wars-galaxy.png'

const TitleStyle = styled.h1`
    font-size: 1.5em;
    text-align: center;
    color: palevioletred;
    height: 200px;
    display:flex;
    justify-content: center;
    align-items: center;
    background-image: url(${StarWarsGalaxy});
    color: #FFD700;
    text-transform: uppercase;
`;

const Wrapper = styled.section`
    width: 100%;
    height: 80vh;
`;

const ContainerStyle = styled.section`
    width: 100vw;
    height: 100vh;
    background: papayawhip;
    border-radius: 5px;
    display: flex;
    padding: 20px;
    box-sizing: border-box;
    margin:0;
    padding:0;
`;

const App = () => {
    return (
        <Wrapper>
            <TitleStyle>
                Star Wars Universe
            </TitleStyle>
            <ContainerStyle>
                <Movies/>
            </ContainerStyle>
        </Wrapper>
    )
}

export default App;